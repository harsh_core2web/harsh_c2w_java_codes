

import java.io.*;

class demo{

 
  public static void main(String [] args)throws IOException{


   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
   System.out.println("Enter number : ");
   int num = Integer.parseInt(br.readLine());
   int temp = num;
   int rev = 0;
   
    while(temp>=1){

        int rem = temp%10;
	rev = rev*10 + rem;
	temp/=10;
  }
  System.out.println("Reverse of "+num+" is "+rev);

}

}


