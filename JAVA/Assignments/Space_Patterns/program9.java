

import java.io.*;

class space_pattern{


    public static void main(String [] args)throws IOException{


          BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            
         System.out.println("Enter Number of Rows : ");  
         int rows = Integer.parseInt(br.readLine());
                
         for(int i = 1; i<=rows;i++){
                  int num=rows+64;
             for(int j = 1; j<=i-1; j++ ){
          
                   System.out.print("  ");

	     }

	     for(int k=rows; k>=i; k-- ){

                 System.out.print((char)(num)+" ");
	         num--;      	 

	     }
            
	    System.out.println(); 

	 }


    }


}


