

import java.io.*;

class space_pattern{


    public static void main(String [] args)throws IOException{


          BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            
         System.out.println("Enter Number of Rows : ");  
         int rows = Integer.parseInt(br.readLine());
          
         for(int i = 1; i<=rows;i++){

                  int num = rows;
              
             for(int j = rows-1; j>=i; j-- ){
          
                   System.out.print("  ");

	     }

	     for(int k=1; k<=i; k++ ){

                 System.out.print(num+" ");
	         num--;	 

	     }
            
	    System.out.println(); 

	 }



    }


}


