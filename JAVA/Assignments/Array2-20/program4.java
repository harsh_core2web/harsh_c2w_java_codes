import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array  : ");
		int num = sc.nextInt();
		int arr[] =new int[num];

		for(int i = 0;i<arr.length;i++){
			System.out.print("\nEnter value : ");
			arr[i] = sc.nextInt();
		}

		System.out.print("Entered array of characters is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

		System.out.print("\nEnter the number to be searched :");
		int Key = sc.nextInt();
		int idx=-1;

		for(int i=0;i<arr.length;i++){
			if(Key == arr[i]){
				idx=i;
			}
		}

		if(idx!=-1)

				System.out.println("OOO YAAA  "+Key  +" found at index "+idx);
		else
				System.out.println("OOP's "+Key+" not present in the arr .");

	}
}
