import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array  : ");
		int num = sc.nextInt();
		int arr[] =new int[num];

		for(int i = 0;i<arr.length;i++){
			System.out.print("\nEnter value : ");
			arr[i] = sc.nextInt();
		}

		System.out.print("Entered array of characters is : ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

		int sum = 0;

		for(int i=0;i<arr.length;i++){
			if(i%2==1){
				sum+=arr[i];
			}
		}
		System.out.println("\nSum of Odd Elements are : "+sum);
		}
}
