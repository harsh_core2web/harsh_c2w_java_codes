import java.util.Scanner;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array  : ");
		int num = sc.nextInt();
		int arr[] =new int[num];

		for(int i = 0;i<arr.length;i++){
			System.out.print("\nEnter value : ");
			arr[i] = sc.nextInt();
		}

		int sum = 0;
		//System.out.println("Even elements are : ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%3==0){
				sum+=arr[i];
			}
		}
		System.out.println("Sum is elements divisible by 3 are :  "+sum);

	}
}
