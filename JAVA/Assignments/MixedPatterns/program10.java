import java.io.*;

class pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader( new InputStreamReader ( System.in));
                System.out.print("Enter a number: ");
                int num = Integer.parseInt(br.readLine());
		
		int rem = 0;

		while(num >= 1){
		
			rem = num % 10;

			if(rem % 2 == 1){
				
				System.out.print(rem*rem + ",");
			}
			num = num/10;
		}


                
	}
}
