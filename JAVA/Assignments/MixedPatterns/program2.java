import java.io.*;

class pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader( new InputStreamReader ( System.in));
                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());


			int start = rows;
                for(int i = 1 ; i <= rows ; i++){
			
			int num = start;

                        for(int j = 1 ; j <= rows ; j++){

                                System.out.print((char)(64+rows) +""+ num-- + " ");

                       	}

			start++;

                        System.out.println();
                }
        }
}
