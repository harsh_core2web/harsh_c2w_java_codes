
import java.io.*;

class demo{

 
  public static void main(String [] args)throws IOException{


   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
   System.out.println("Enter no of rows : ");
   int rows = Integer.parseInt(br.readLine());
   for(int i=rows;i>=1;i--){ 
              int num = i;
          for(int j=1;j<=i;j++){
         
               if(i%2==1){
                 System.out.print((char)(num+96)+" ");
	       }
               else{ 
		  System.out.print((char)(num+64)+" ");
	       }
	       
	     num--;
	  } 
        System.out.println();
    
    }

  }


}

